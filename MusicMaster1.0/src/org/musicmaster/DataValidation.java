package org.musicmaster;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * this Class validates the Email input
 * @author D.Kos
 *
 */
public class DataValidation {
	@SuppressWarnings("unused")
	private Pattern pattern;
	@SuppressWarnings("unused")
	private Matcher matcher;

	/** the Pattern with Email Regular Expression */
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
		    Pattern.compile("^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"
		    		, Pattern.CASE_INSENSITIVE);		

	
	/**
	 * Validates User Email with RegularExpressions
	 * @param emailStr
	 * @return true (ONLY if Matches Pattern)
	 */
	public static boolean validateEmail(String emailStr) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
		return matcher.find();
	}

}
