package org.musicmaster;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MetaEventListener;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequencer;

/**
 * This MidiPlayer Class creates and controls sequencer
 * gets length, playPosition
 * @author D.Kos
 *
 */
public class MidiPlayer implements MetaEventListener {
	// MusicMaster V2 : The drum track in the example Midi file (NOT implemented)
//	private static final int DRUM_TRACK = 1;

	@SuppressWarnings("unused")
	private String filePath;
	private Sequencer sequencer;
	
	
	public static long determineLength(String path){
		return new MidiPlayer(path).getLength();
	}

	/**
	 * Midiplayer sets sequencer with InputStream(filePath)
	 * @param filePath
	 * @throws MidiUnavailableException
	 * @throws IOException
	 * @throws InvalidMidiDataException
	 */
	public MidiPlayer(String filePath) {
		this.filePath = filePath;
		try {
			sequencer = MidiSystem.getSequencer();
			sequencer.open();
			BufferedInputStream is = new BufferedInputStream(
					new FileInputStream(new File(filePath)));
			sequencer.setSequence(is);
		} catch (Exception e) {
			App.getInstance().showAlert("Midi Player could not start.");
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Midi Player could not start.", e);
		}
	}
	

	/** sequencer controls : */
	public void play() {
		sequencer.start();
	}	
	public void stop() {
		sequencer.stop();
	}
	public boolean isPlaying() {
		return this.sequencer.isRunning();
	}
	public void sequencerClose() {
		sequencer.stop();
		sequencer.close();
	}
	
	
	/** gets the Song length
	 *  @return milisek 
	 *  */
	public long getLength() {
		return sequencer.getMicrosecondLength() / 1000;
	}
	
	/** gets the Play position of Song
	 * @return milisek
	 */
	public long getPlayPosition() {
		return sequencer.getMicrosecondPosition() /1000;
	}

	@Override
	public void meta(MetaMessage meta) {
		// MusicMaster V2 : Instrument played by sequencer (DRUM,GUITAR,VOCALS,..)
	}
	
}
