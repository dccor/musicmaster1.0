package org.musicmaster;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * this Class generates and validates Password (MD5, SHA1PRNG)
 * @author D.Kos
 */
public class PasswordGenerator {

	/**
	 * create Password hash
	 * @param passwordToHash
	 * @param salt
	 * @return generated Password
	 */
	public static String getSecurePassword(String passwordToHash, byte[] salt)
    {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            /** Add password bytes to digest */
            md.update(salt);
            /** Get the hash's bytes */ 
            byte[] bytes = md.digest(passwordToHash.getBytes());
            /** This bytes[] has bytes in decimal format; */
            /** Convert it to hexadecimal format */
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            /** Get complete hashed password in hex format */
            generatedPassword = sb.toString();
        } 
        catch (NoSuchAlgorithmException e) {
        	App.getInstance().showAlert("Could not generate Password.");
        	Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not generate Password.", e);
        }
        return generatedPassword;
    }
	
	/**
	 * generate Secure Random 16byte salt (SHA1PRNG)
	 * @return  byte[] salt
	 */
	public static byte[] getSalt()  {
        try {
			/** a SecureRandom generator */
			SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "SUN");
			byte[] salt = new byte[16];
			/** Get a random salt */
			sr.nextBytes(salt);
			return salt;
		} catch (NoSuchAlgorithmException e) {
			App.getInstance().showAlert("Could not generate salt.");
        	Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not generate salt. NoSuchAlgorithmException.", e);
        	return null;
		} catch (NoSuchProviderException e) {
			App.getInstance().showAlert("Could not generate Password.");
        	Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not generate salt. NoSuchProviderException.", e);
        	return null;
		}
    }
	
	/** run getSecurePassword and return true/false
	 * 
	 * @param passwordToHash
	 * @param salt
	 * @param hashedPassword
	 * @return boolean
	 */
	public static boolean validatePassword(String passwordToHash, byte[] salt, String hashedPassword) {
		return getSecurePassword(passwordToHash, salt).equals(hashedPassword);
	}

}
