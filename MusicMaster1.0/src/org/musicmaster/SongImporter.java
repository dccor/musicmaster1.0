package org.musicmaster;

import java.io.File;
import java.io.FilenameFilter;

/**
 * this Class imports the Songs
 * @author D.Kos
 *
 */
public class SongImporter {
	/**
	 * create directories from file.list with FilenameFilter
	 * return if File(). isDirectory 
	 * @param rootPath
	 */
	static void importSongs(String rootPath) {
		File file = new File(rootPath);
		@SuppressWarnings("unused")
		String[] directories = file.list(new FilenameFilter() {
		  @Override
		  public boolean accept(File current, String name) {
		    return new File(current, name).isDirectory();
		  }
		});
	}
}
