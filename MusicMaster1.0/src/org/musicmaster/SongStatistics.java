package org.musicmaster;

import java.time.LocalDateTime;

/**
 * this Class holds the SongStatistic Members
 * Constructor with Attributes
 * @author D.Kos
 *
 */
public class SongStatistics {
	
	public String title;
	public String artist;
	public String genre;
	public LocalDateTime played_at;

	/**
	 * Constructor for SongStatistics
	 * @param title
	 * @param artist
	 * @param genre
	 * @param played_at
	 */
	public SongStatistics(String title, String artist, String genre, LocalDateTime played_at) {
		super();
		this.title = title;
		this.artist = artist;
		this.genre = genre;
		this.played_at = played_at;
	}
}
