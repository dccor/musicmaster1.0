package org.musicmaster.models;

/**
 * this Class hold the Instrument ENUM
 * @author D.Kos
 *
 */
public enum Instrument {
	GUITAR("Guitar"), PIANO("Piano"), BASS("Bass");

	private final String value;

	private Instrument(final String value) {
		this.value = value;
	}

	/** returns the value from ENUM */
	@Override
	public String toString() {
		return value;
	}
}
