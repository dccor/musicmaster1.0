package org.musicmaster.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Song Class is @DatabaseTable "statistics"
 * @author D.Kos
 *
 */
@DatabaseTable(tableName = "statistics")
public class Statistics {
	
	@DatabaseField(canBeNull = false, foreign = true)
	private User user;
	@DatabaseField(canBeNull = false, foreign = true)
	private Song song;
	// UnixTime (in sec)
	@DatabaseField(canBeNull = false)
	private long time;
	


	public Statistics() {
	    /** ORMLite needs a no-arg constructor */ 
	}
	
	public Statistics(User user, Song song, long time){
		this.user = user;
		this.song = song;
		this.time = time;
	}
	
	

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Song getSong() {
		return song;
	}

	public void setSong(Song song) {
		this.song = song;
	}
	public long getTime() {
		return time;
	}
	
	public void setTime(long time) {
		this.time = time;
	}
	
}