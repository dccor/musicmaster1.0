package org.musicmaster.views;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.musicmaster.App;
import org.musicmaster.DataValidation;
import org.musicmaster.PasswordGenerator;
import org.musicmaster.models.Instrument;
import org.musicmaster.models.User;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Controller Class for edituserdata.fxml
 * gets all userdata sets in Text/ComboBox-Fields and updates User with OK Button
 * @author D.Kos
 *
 */
public class EditUserdataController {

	@FXML
	private Label infoLabel;
	@FXML
	private TextField usernameField;
	@FXML
	private PasswordField passwordField;
	@FXML
	private TextField firstNameField;
	@FXML
	private TextField lastNameField;
	@FXML
	private TextField emailField;
	@FXML
	private ComboBox<Instrument> instrumentEnumCmb;
	@FXML
	private Button btnOk;

	private Stage dialogStage;
	private boolean okClicked = false;

	protected User currentUser;

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {
		/** add ENUM Instruments to Combobox */
		instrumentEnumCmb.getItems().setAll(Instrument.values());
		this.currentUser = App.getInstance().getCurrentUser();
		setUserdata(currentUser);
	}
	
	
	/** if isInputValid sets userData and updates to userTable */
	@FXML
	void handleOk(ActionEvent event){
		try {
			if (isInputValid()) {
				currentUser.setUsername(usernameField.getText());
				String passwordToHash = passwordField.getText();
				if (!passwordToHash.equals("")) {
					byte[] salt = PasswordGenerator.getSalt();
					String password = PasswordGenerator.getSecurePassword(passwordToHash, salt);
					currentUser.setPassword(password);
					currentUser.setSalt(salt);
				}
				currentUser.setFirstName(firstNameField.getText());
				currentUser.setLastName(lastNameField.getText());
				currentUser.setEmail(emailField.getText());
				currentUser.setInstrument(instrumentEnumCmb.getValue());
				App.getInstance().getDb().updateUser(currentUser);

				okClicked = true;
				dialogStage.close();
			}
		} catch (Exception e) {
			App.getInstance().showAlert("Could not update Userdata.");
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not update Userdata.", e);
		}
	}

	/** Called when the user clicks cancel -> closes the DialogStage */
	@FXML
	void handleCancel(ActionEvent event) {
		dialogStage.close();
	}

	/**
	 * Sets the Userdata to be edited in the dialog.
	 * @param user
	 */
	public void setUserdata(User user) {
		usernameField.setText(user.getUsername());
		firstNameField.setText(user.getFirstName());
		lastNameField.setText(user.getLastName());
		emailField.setText(user.getEmail());
		instrumentEnumCmb.getSelectionModel().select(currentUser.getInstrument());
	}

	/**
	 * Validates the user input in the text fields. shows Alert Dialog
	 * @return true if the input is valid
	 */
	private boolean isInputValid() {
		String errorMessage = "";
		/** validates Email Expression */
		if (!DataValidation.validateEmail(emailField.getText())) {
			errorMessage += "Not a valid Email adress\n";
		}		
		if (usernameField.getText() == null) {
			errorMessage += "invalid Username!\n";
		}
		String pass = passwordField.getText();
		if (!(pass == null || pass.equals(""))) {
			if (pass.length() <= 4) {
				errorMessage += "invalid Password!\n";
			}
		}
		if (firstNameField.getText() == null || firstNameField.getText().length() <= 2) {
			errorMessage += "invalid First Name!\n";
		}
		if (lastNameField.getText() == null || lastNameField.getText().length() <= 2) {
			errorMessage += "invalid Last Name!\n";
		}
		if (errorMessage.length() == 0) {
			return true;
			
		} else {
			/** Show the error message. */
			Alert alert = new Alert(AlertType.INFORMATION);
			DialogPane dialogPane = alert.getDialogPane();
			dialogPane.getStylesheets().add(
					   getClass().getResource("DarkTheme.css").toExternalForm());
			alert.initOwner(dialogStage);
			alert.setTitle("Invalid Fields");
			alert.setHeaderText("Please correct your input");
			alert.setContentText(errorMessage);
			alert.showAndWait();

			return false;
		}
	}

	/** Sets the new dialog stage */
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	/**@return true, if the user clicked OK, false otherwise. */
	public boolean isOkClicked() {
		return okClicked;
	}

}
