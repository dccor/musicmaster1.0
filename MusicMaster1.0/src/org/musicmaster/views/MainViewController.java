package org.musicmaster.views;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.musicmaster.App;
import org.musicmaster.models.Song;
import org.musicmaster.models.SongProperties;
import org.musicmaster.models.User;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;

/**
 * Controller Class for mainView.fxml
 * @author D.Kos
 *
 */
public class MainViewController {

	@FXML
	private Label usernameLabel;
	@FXML
	private Label firstnameLabel;
	@FXML
	private Label lastnameLabel;
	@FXML
	private Label sessionLabel;
	@FXML
	private Label emailLabel;
	@FXML
	private Label instrumentLabel;
	@FXML
	private TableView<SongProperties> songTable;
	@FXML
	private TableColumn<SongProperties, String> titelnameCol;
	@FXML
	private TableColumn<SongProperties, String> artistCol;
	@FXML
	private TableColumn<SongProperties, String> genreCol;
	@FXML
    private TableColumn<SongProperties, Integer> diffCol;
	@FXML
	private TableColumn<SongProperties, Long> songlengthCol;
	@FXML
	private Button btnSortDifficulty1;
	@FXML
	private Button btnSortName;
	@FXML
	private ComboBox<String> cmbFilterGenre;
	@FXML
	private Label songtitleLabel;
	@FXML
	private Label artistLabel;
	@FXML
	private Label genreLabel;
	@FXML
	private Label lengthLabel;
	@FXML
	private Label difficultyLabel;
	@FXML
	private Label LoginTimeLbl;
	@FXML
	private Label loginTimeLabel;
	@FXML
	private Label songsLbl;
	@FXML
	private Label songCntLabel;


	/** List of Song */
	private List<Song> songList;
	
	/** Observable List with SongsProperties */
	private ObservableList<SongProperties> songListObserv;
	/** Property for selected Song */
	private ObjectProperty<SongProperties> selectedSong;

	/** Constructor instantiate selectedSong Property and add FXCollection List */
	public MainViewController(){
		selectedSong = new SimpleObjectProperty<>();
		songListObserv = FXCollections.observableArrayList();
	}

	/** initialize - called after Constructor */
	@FXML
	private void initialize(){
		/** set currentUser and songList from database */
		User currentUser = App.getInstance().getCurrentUser();
		this.songList = App.getInstance().getDb().loadSongList();
		
		/** create genres for ComboBox-filter */ 
		ArrayList<String> genres = new ArrayList<>();
		for (Song song : songList) {
			songListObserv.add(new SongProperties(song));
			if (!genres.contains(song.getGenre())) {
				genres.add(song.getGenre());
			}
		}
		
		/** for sorted and filtered Lists: */
		FilteredList<SongProperties> filteredSongs = new FilteredList<>(songListObserv, p -> true);
		SortedList<SongProperties> sortedSongs = new SortedList<>(filteredSongs);
		sortedSongs.comparatorProperty().bind(songTable.comparatorProperty());
		songTable.setItems(sortedSongs);
		
		/** TableView: */ 
		titelnameCol.setCellValueFactory(cellData -> cellData.getValue().titleProperty());
		artistCol.setCellValueFactory(cellData -> cellData.getValue().artistProperty());
		genreCol.setCellValueFactory(cellData -> cellData.getValue().genreProperty());
		diffCol.setCellValueFactory(cellData -> cellData.getValue().difficultyProperty().asObject());
				 
		
		/** Listen for selection changes and show the song details when changed. */
		songTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			if (selectedSong.get() != newValue) {
				selectedSong.set(newValue);
				showSongDetails(newValue);
			}
		});
		usernameLabel.setText(currentUser.getUsername());
		sessionLabel.setText(Integer.toString(currentUser.getSessionCounter()));
		emailLabel.setText(currentUser.getEmail());
		instrumentLabel.setText(currentUser.getInstrument().toString());
		loginTimeLabel.setText(getCurrentTimeFormat());
		songCntLabel.setText(String.valueOf(App.getInstance().getDb().countSongs()));
		
		/** add Item on Index 0 for 'All' selection */
		genres.add(0, "All");
		/** new filtered List with select-Listener to show filtered List */
		ObservableList<String> genreFilterList = FXCollections.observableArrayList(genres);
		cmbFilterGenre.getItems().addAll(genreFilterList);
		cmbFilterGenre.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            filteredSongs.setPredicate(song -> {
                /** If filter text is empty, display all songs */
                if (newValue == null || newValue.isEmpty() || newValue.equals("All")) {
                    return true;
                }
                return newValue.equals(song.getGenre());
            });
        });
		cmbFilterGenre.getSelectionModel().select(0);
	}


	/** get the Song from unique Song id(String) */
	private Song getSongFromId(String id) {
		for (Song song : songList) {
			if (song.getId() == id) {
				return song;
			}
		}
		return null;
	}

	/** sets Song Details for selected Song from SongProperties */
	private void showSongDetails(SongProperties songProp) {
		Song song;
		if (songProp != null) {
			song = getSongFromId(songProp.getId());

			songtitleLabel.setText(song.getTitle());
			artistLabel.setText(song.getArtist());
			genreLabel.setText(song.getGenre());
			lengthLabel.setText(song.getPrettyLength());
			difficultyLabel.setText(Integer.toString(song.getDifficulty()));
		} else {
			songtitleLabel.setText("");
			artistLabel.setText("");
			genreLabel.setText("");
			lengthLabel.setText("");
			difficultyLabel.setText("");
		}
	}

	
	/** Button ActionEvent to call showStatistics */
	@FXML
	private void handleStatistics(ActionEvent event) {
		App.getInstance().showStatistics();
	}

	/** Button ActionEvent to call showEditUserdata */
	@FXML
	public void handleEditUserdata(ActionEvent event) {
		App.getInstance().showEditUserdata(App.getInstance().getCurrentUser());

	}

	/** generates SessionID, logoutUser(sets currentUser=null),  shows Startscreen*/
	@FXML
	public void handleLogout(ActionEvent event) {
		App app = App.getInstance();
		app.logoutUser();
		app.showStartScreen();
	}

	/** Button ActionEvent
	 *  load selected Song and call showMediaplayer (selectedSong)
	 * @param event
	 */
	@FXML
	public void handleStartLession(ActionEvent event) {
		if (selectedSong.getValue() != null) {
			Song song;
			String id = selectedSong.getValue().getId();
			song = App.getInstance().getDb().loadSongById(id);
			App.getInstance().showMediaplayer(song);
		} else {
			App.getInstance().showAlert("Please select a Song.");
		}
	}

	/** ActionEvent on MouseClick - showAbout Dialog */
	@FXML
	void onMouseImage(MouseEvent event) {
		showAboutDialog();
	}

	/**
	 * @return a Observable List
	 */
	public ObservableList<SongProperties> getSongList() {
		return songListObserv;
	}

	/**
	 * get current Time -called for LoginTime (Millisec)
	 * @return SimpleDateFormat HH:mm:ss
	 */
	public String getCurrentTimeFormat() {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		return sdf.format(System.currentTimeMillis());
	}

	/** About Dialog */
	/** view AboutDialog with MouseClick on ImageView */
	public void showAboutDialog() {
		Alert alert = new Alert(AlertType.INFORMATION);
		DialogPane dialogPane = alert.getDialogPane();
		dialogPane.getStylesheets().add(getClass().getResource("DarkTheme.css").toExternalForm());
		alert.setResizable(false);
		alert.initOwner(App.getInstance().stage);
		alert.setTitle("About");
		alert.setHeaderText("Music Master v1.0");
		alert.setContentText("2017 Copyright Dalibor Kos");
		alert.showAndWait();
	}

}
