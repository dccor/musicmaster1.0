package org.musicmaster.views;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.musicmaster.App;
import org.musicmaster.MidiPlayer;
import org.musicmaster.models.Song;

import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

/**
 * Controller Class for mediaplayer.fxml
 * @author D.Kos
 *
 */
public class MediaplayerController {

	@FXML
	private Label songtitleLabel;
	@FXML
	private Label artistLabel;
	@FXML
	private Label genreLabel;
	@FXML
	private Label lengthLabel;
	@FXML
	private ImageView imgNotes;
	@FXML
    private AnchorPane imgPane;
	@FXML
	private ToggleButton btnPlay;
	@FXML
    private ToggleButton btnFullscreen;
	@FXML
	private Button btnCancel;
	@FXML
	private VBox vboxButtons;

	private boolean isCancel;
	private boolean statisticsTimeSaved;
	
	public List<Image> notePages = new ArrayList<>();
	private int currentPage;
	
	/** Class Fields*/
	private Song song;
	private MidiPlayer midiplayer;


	/** handle Toggle Button event - Plays / Pauses the Song */
	@FXML
	void handlePlay(Event event) {
		if (midiplayer.isPlaying()) {
			midiplayer.stop();
			btnPlay.setText("  Play  ");
			btnPlay.setStyle("-fx-text-fill: #66CDAA;");
		} else {
			midiplayer.play();
			timerTick();
			btnPlay.setText(" Pause");
			btnPlay.setStyle("-fx-text-fill: #c1cf63;");
		}
	}
	
	/** handle Toggle Button event - sets stage Fullscreen true / false  */
	@FXML
    void handleFullscreen(Event event) {
		if(!App.getInstance().stage.isFullScreen()){
		App.getInstance().setStageFullScreen();
		btnFullscreen.setText("Esc Fullscreen");
		vboxButtons.setSpacing(60);
		} else {
			App.getInstance().setStageFullScreenfalse();
			btnFullscreen.setText(" Fullscreen ");
			vboxButtons.setSpacing(40);
		}
			
		

    }
	
	
	/** checks if notePages has next Page*/
	private boolean hasNextPage(){
		if(currentPage == notePages.size() - 1){
			return false;
		}else return true;
	}
	

	Timer timer = new java.util.Timer();
	
	/**
	 * if midiplayer is playing, 
	 * at half of Songlength createsUserStatisticEntry
	 * if hasNextPage -> showNotes with next image
	 * shows lengthLabel with playPosition in prettyPrintMilliSeconds
	 * timerTask with 1000 milliseconds
	 */
	private void timerTick() {
		if (!midiplayer.isPlaying()) {
			return;
		}		
		long playPosition = midiplayer.getPlayPosition();
 
		if (!this.statisticsTimeSaved && playPosition >= this.song.getLength() / 2) {
			try {
				App.getInstance().getDb().createUserStatisticsEntry(App.getInstance().getCurrentUser(), this.song);
				this.statisticsTimeSaved = true;
			} catch(Exception e) {
				Logger.getLogger(MediaplayerController.class.getName()).log(Level.SEVERE, "Could not createUserStatisticsEntry", e);
			}
		}

		if(hasNextPage()) {
			long changeTime = (long)(song.getNotePageChangeTimes().get(currentPage)) * 1000;
			if(playPosition > changeTime){
				showNotes(currentPage + 1);
			}
		}	
		lengthLabel.setText(App.prettyPrintMilliSeconds(playPosition));
		
		/** Start a new timer. */
		timer.schedule(new TimerTask() {
		    public void run() {
		         Platform.runLater(new Runnable() {
		            public void run() {
		                timerTick();
		            }
		        });
		    }
		}, 1000);
	}

	/** Close Button stops Midiplayer and showMainView */
	@FXML
	void handleCancel(Event event) {
		isCancel = true;
		try {
			midiplayer.stop();
		} catch (Exception e) {
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Midi Player could not stop.", e);
		}
		App.getInstance().showMainView();
	}
	
	/** Stop Button stops Midiplayer and showMainView */
	@FXML
	void handleStop(Event event) {
		isCancel = true;
		try {
			midiplayer.stop();
			midiplayer.sequencerClose();
			btnPlay.setText("  Play  ");
			btnPlay.setStyle("-fx-text-fill: #66CDAA;");
			init(song);		
		} catch (Exception e) {
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Midi Player could not stop.", e);
		}
	}
	
	public boolean isCancel(){
		return this.isCancel;
	}

	/** sets Image with notePageChangeTime */
	private void showNotes(int notePageNumber) {
		imgNotes.setImage(notePages.get(notePageNumber));
		currentPage = notePageNumber;
	}

	
	/** init called by showMediaplyer-handle StartLession from MainView
	 * fit Image size, sets the Song to be played
	 * sets SongLabels - length with getPrettyLength 
	 * @param song
	 */
	public void init(Song song){
		imgNotes.fitWidthProperty().bind(imgPane.widthProperty());
		imgNotes.fitHeightProperty().bind(imgPane.heightProperty());
		this.statisticsTimeSaved = false;
		this.song = song;
		try {
			createImage();
		} catch (Exception e1) {
			App.getInstance().showAlert("Could not create Image.");
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not create Image.", e1);
		}
		this.songtitleLabel.setText(song.getTitle());
		this.artistLabel.setText(song.getArtist());
		this.genreLabel.setText(song.getGenre());
		String length = song.getPrettyLength();
		this.lengthLabel.setText(length);

		/** new Midiplayer with file from unique userId AssetPath */
		try {
			String filePath = App.getInstance().getAssetPath("/songs/" + song.getId() + "/song.mid");
			midiplayer = new MidiPlayer(filePath);
		} catch (Exception e) {
			App.getInstance().showAlert("Could not find Song Files in Asset Path.");
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not find Song Files in Asset Path.", e);
		}

		showNotes(0);
	}
	
	/** adds all images from path to Image-List  */
	private void createImage() throws Exception {
		File songfile = Paths.get(App.getInstance().getAssetPath("/songs/" + song.getId() + "/pdf.pdf")).toFile();
	    List<Image> mylist = new ArrayList<Image>();
	    PDDocument document = PDDocument.load(songfile);
	    PDFRenderer pdfRenderer = new PDFRenderer(document);
	    for (int page = 0; page < document.getNumberOfPages(); ++page)
	    { 
	        try {
	        	mylist.add(SwingFXUtils.toFXImage(pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB), null));
			} catch (IOException e) {
				App.getInstance().showAlert("Could not Render Image.");
				Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not Render Image.", e);
			}
	    }
	    document.close();
	    notePages.addAll(mylist);
	}
	
	
	/** getter for Song length */
	public long getSonglength() {
		return this.song.getLength();
	}

}
