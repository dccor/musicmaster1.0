package org.musicmaster.views;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.musicmaster.App;
import org.musicmaster.DataValidation;
import org.musicmaster.PasswordGenerator;
import org.musicmaster.models.Instrument;
import org.musicmaster.models.User;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Controller for registerDialog.fxml
 * @author D.Kos
 *
 */
public class RegisterDialogController {

	@FXML
	private Label infoLabel;
	@FXML
	private TextField usernameField;
	@FXML
	private PasswordField passwordField;
	@FXML
	private TextField firstNameField;
	@FXML
	private TextField lastNameField;
	@FXML
	private TextField emailField;
	@FXML
	private ComboBox<Instrument> instrumentEnumCmb;
	@FXML
	private Button btnOk;

	/** The dialog stage. */
	private Stage dialogStage;

	/** The ok Button clicked. */
	private boolean okClicked = false;

	
	public RegisterDialogController() {
	}

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {
		usernameField.setPromptText("4 - 20 characters ");
		passwordField.setPromptText("min 5 characters ");
		firstNameField.setPromptText("min 3 characters ");
		lastNameField.setPromptText("min 3 characters ");
		Platform.runLater(() -> usernameField.requestFocus());

		// add ENUM Items to Combobox
//		instrumentEnumCmb.getItems().setAll(FXCollections.observableArrayList(instrument.values()));
		instrumentEnumCmb.getItems().setAll(Instrument.values());
		instrumentEnumCmb.getSelectionModel().select(0);
		
		
	}

	/**
	 * Sets the new dialog stage
	 * @param dialogStage
	 */
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	/**
	 * @return true, if the user clicked OK, false otherwise.
	 */
	public boolean isOkClicked() {
		return okClicked;
	}

	/**
	 * Called when the user clicks OK Only if isInputValid sets User Data from
	 * Fields if okClicked
	 * @throws Exception 
	 */
	@FXML
	private void handleOk() throws Exception {
		if (isInputValid()) {
		String username = usernameField.getText();
		String passwordToHash = passwordField.getText();
		// get ENUM String
		String sinstr = instrumentEnumCmb.getSelectionModel().getSelectedItem().toString().toUpperCase();
		Instrument instr = Instrument.valueOf(sinstr);

		byte[] salt = PasswordGenerator.getSalt();
		String password = PasswordGenerator.getSecurePassword(passwordToHash, salt);
		try {
			App.getInstance().getDb().createUser(new User(
					username, firstNameField.getText(), lastNameField.getText(),
					emailField.getText()
					, password, salt, instr, 0, 0));
		} catch (Exception e) {
			App.getInstance().showAlert("Could not create User. Try again and check Username.");
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not create database entry user.", e);
		}		
			okClicked = true;
			dialogStage.close();
		}
	}


	/**
	 * Validates the user input in the text fields shows Alert messages
	 * @return true if no error message
	 */
	private boolean isInputValid() {
		String errorMessage = "";

		if (!DataValidation.validateEmail(emailField.getText())) {
			errorMessage += "Not a valid Email adress\n";
		}

		if (usernameField.getText() == null || usernameField.getText().length() <= 3  || usernameField.getText().length() >= 19) {
			errorMessage += "invalid Username!\n";
		}
		if (passwordField.getText() == null || passwordField.getText().length() <= 4) {
			errorMessage += "invalid Password!\n";
		}

		if (firstNameField.getText() == null || firstNameField.getText().length() <= 2 || firstNameField.getText().length() >= 19) {
			errorMessage += "invalid First Name!\n";
		}
		if (lastNameField.getText() == null || lastNameField.getText().length() <= 2  || lastNameField.getText().length() >= 19) {
			errorMessage += "invalid Last Name!\n";
		}
		if (errorMessage.length() == 0) {
			return true;
		} else {
			/** show Alert Message Dialog*/
			Alert alert = new Alert(AlertType.ERROR);
			DialogPane dialogPane = alert.getDialogPane();
			dialogPane.getStylesheets().add(
			   getClass().getResource("DarkTheme.css").toExternalForm());
			alert.initOwner(dialogStage);
			alert.setTitle("Invalid Fields");
			alert.setHeaderText("Please correct your input");
			alert.setContentText(errorMessage);
			alert.showAndWait();
			return false;
		}
	}

	/** Called when the user clicks cancel -> closes the DialogStage */
	@FXML
	private void handleCancel() {
		usernameField.clear();
		passwordField.clear();
		firstNameField.clear();
		lastNameField.clear();
		emailField.clear();
		dialogStage.close();
	}

}
