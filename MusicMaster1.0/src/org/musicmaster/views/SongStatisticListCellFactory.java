package org.musicmaster.views;


import java.time.format.DateTimeFormatter;

import org.musicmaster.SongStatistics;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class SongStatisticListCellFactory {


	/**
	 * Default ListView display.
	 *
	 * @return the callback
	 */
	public static Callback<ListView<SongStatistics>, ListCell<SongStatistics>> defaultSongStatisticsListView(){
		
		// implement callback, ListView calls when create new Cells
		return new Callback<ListView<SongStatistics>, ListCell<SongStatistics>>() {
			
			@Override
			public ListCell<SongStatistics> call(ListView<SongStatistics> param) {
				// implement own ListCell-Class and return
				return new ListCell<SongStatistics>(){
					@Override
					protected void updateItem(SongStatistics item, boolean empty) {
						// BasicClass-implement for updateItem (Example: to set Styles for selected  Item aso.)
						super.updateItem(item, empty);
						
						if(empty || item == null){
							// show nothing
							setText(null);
						}
						else{
							DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy  HH:mm");
							String formatTime = item.played_at.format(formatter);
							setText(item.artist.toString() + "  |  " + item.title + "  @  " + formatTime );
						}
					}
				};
			}
		};
	}

}
