package org.musicmaster.views;

import java.time.Instant;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.musicmaster.App;
import org.musicmaster.models.User;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;


/**
 * Controller for startscreen.fxml
 * @author D.Kos
 *
 */
public class StartScreenController {

    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;
	@FXML
	private Button loginButton;
	@FXML
	private Label message;
	@FXML
	private ImageView iv1;


	/** initialize message, Fields, set Focus and defaultButton*/
	@FXML
	public void initialize() {
		message.setText("Please Login:");
		message.setTextFill(Color.rgb(30, 30, 220));
		usernameField.setPromptText("Username");
		passwordField.setPromptText("Password");
		Platform.runLater(() -> usernameField.requestFocus());
		loginButton.setDefaultButton(true);
	}

	/** set the message Label Text */
	public void setMessage(String msg){
		this.message.setText(msg);
	}
	
	
	/** create a new User and show RegisterDialog */
	@FXML
	void handleRegister(ActionEvent event) {
				User newUser = new User();
				newUser = App.getInstance().showRegisterDialog(newUser);
	}

	/** Login-Button handler
	 *  sets currentUser in App
	 *  authenticateUser (username, password)
	 *  set StartSessionTime (now)
	 * @param event
	 */
	@FXML
	void handlelLogin(ActionEvent event){		
		try {
			String username = usernameField.getText();
			String password = passwordField.getText();
			User currentUser;
			currentUser  = App.getInstance().getDb().authenticateUser(username, password);
			if(currentUser != null){
				App.getInstance().setCurrentUser(currentUser);
				App.getInstance().showMainView();
				passwordField.clear();
				App.getInstance().setStartSessionTime(Instant.now().toEpochMilli());
			}
			else {
				passwordField.clear();
				message.setText("Please try again:");
				message.setTextFill(Color.rgb(255,50,0));
			}		
		} catch (Exception e) {
			App.getInstance().showAlert("Could not load User.");
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Login Exception", e);
		}
		
	}
	

	
	
}
