package org.musicmaster.views;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.musicmaster.App;
import org.musicmaster.SongStatistics;
import org.musicmaster.UserStatistics;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

/**
 * Controller Class for statistics.fxml all Statistics data and views
 * @author D.Kos
 *
 */
public class StatisticsController {

	@FXML
	private Label usernameLabel;
	@FXML
	private Label lessionTimeLabel;
	@FXML
	private Label totalLessionsLabel;
	@FXML
	private Label totalSessionTimeLabel;
	@FXML
	private BarChart<String, Integer> barChart;
	@FXML
	private CategoryAxis xAxis;
	@FXML
	private NumberAxis yAxis;
	@FXML
	private ListView<SongStatistics> statListView;

	private long lessionTime;

	@FXML
	private void initialize() {
		int userId = App.getInstance().getCurrentUser().getId();
		UserStatistics stats = App.getInstance().getDb().getUserGenreStatistics(userId);
		HashMap<String, Integer> hm = stats.genreCounts;
		
		/**ListView SongStatistics */
		ObservableList<SongStatistics> songStats = FXCollections.observableArrayList(); 
		songStats = App.getInstance().getDb().getSongsPlayedByUser(userId);
		statListView.getItems().addAll(songStats);
		statListView.setCellFactory(SongStatisticListCellFactory.defaultSongStatisticsListView());

		/** Barchart View settings */
		int maximumGengreCount = 5;
		ArrayList<String> genres = new ArrayList<>();
		for (Entry<String, Integer> entry : hm.entrySet()) {
			String key = entry.getKey();
			genres.add(key);
			Integer value = entry.getValue();

			XYChart.Series<String, Integer> series1 = new XYChart.Series<>();
			series1.setName(key);
			series1.getData().add(new XYChart.Data<>(key, value));
			barChart.getData().add(series1);
			if (value > maximumGengreCount) {
				maximumGengreCount = value;
			}
		}

		yAxis.setUpperBound(maximumGengreCount);
		xAxis.setCategories(FXCollections.<String>observableArrayList(genres));
		yAxis.setLabel("Songs played");
		barChart.setTitle("Music Genres you played: ");

		try {
			totalSessionTimeLabel
					.setText(getLessionTimeFormat(App.getInstance().getCurrentUser().getTotalSessionTime()));

		} catch (Exception e1) {
			App.getInstance().showAlert("Could not get Userdata.");
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not get Userdata TotalSessionTime.", e1);
		}
		totalLessionsLabel.setText(Long.toString(stats.lessonCount));
		usernameLabel.setText(App.getInstance().getCurrentUser().getUsername());
		int userid = App.getInstance().getCurrentUser().getId();
		lessionTime = App.getInstance().getCurrentUser().getTotalLessionTime();
		try {
			lessionTimeLabel.setText(getLessionTimeFormat(this.lessionTime));
		} catch (Exception e) {
			App.getInstance().showAlert("Could not get Lession Duration.");
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not get Lession Duration.", e);
		}
		
		App.getInstance().getDb().getUserGenreStatistics(userid);
	}
	

	@FXML
	void handleClose(ActionEvent event) {
		App.getInstance().showMainView();
	}

	/**
	 * get lession Time and Return in a readable view.
	 *
	 * @return String with HH:mm:ss
	 * @throws Exception
	 */
	public String getLessionTimeFormat(long lessionTime) throws Exception {
		String h = "00";
		String m = "00";
		String s = "00";
		long calltime = lessionTime / 1000L;
		if (calltime / 3600L > 0L) {
			int a = (int) (calltime / 3600L);
			h = Integer.toString(a);
			if (h.length() == 1) {
				h = "0" + h;
			}
			calltime -= 3600 * a;
		}
		if (calltime / 60L > 0L) {
			int a = (int) (calltime / 60L);
			m = Integer.toString(a);
			if (m.length() == 1) {
				m = "0" + m;
			}
			calltime -= 60 * a;
		}
		s = Long.toString(calltime);
		if (s.length() == 1) {
			s = "0" + s;
		}
		return h + ":" + m + ":" + s;
	}

}
