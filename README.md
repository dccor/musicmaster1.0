# Project Title

MusicMaster 1.0

This software makes it easy to practice musical instruments and is very intuitive to use.
Over graphical interface the user can perform excercises for a musical instrument.
Simple to use with audio acoustic playback, as well as displayed tabular sheets to improve the learning curve.

## Getting Started

These instructions will get you a copy of the project for testing purposes only.

### Prerequisites

The excecutable exe and Songs in assets Folder are packed in MusicMaster1.zip

```
You can find User Login and Password for testing in the zip Archive
```

### Installing

A Setup Version is also available as well as a MacOS App Version.


## Running the tests

No test in this repository.

## Built With

* [Apache PDFBox 2.0.0 API](https://pdfbox.apache.org/docs/2.0.0/javadocs/) - The API used
* [Apache FontBox » 2.0.0](https://mvnrepository.com/artifact/org.apache.pdfbox/fontbox/2.0.0) - Dependency
* [ormlite](http://ormlite.com/) - Lightweight Object Relational Mapping
* [sqlite](https://www.sqlite.org/) - Database Administration


## Versioning

No Semantic Versioning in this repository.

## Authors

* **Dalibor Kos** - *Initial work* - [dccor](https://github.com/dccor)


## License

Copyright Dalibor Kos**

## Acknowledgments

* Hat tip to Christoph
* Inspiration as loving music and Software Developing
* Google App up next

